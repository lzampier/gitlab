/**
 * Temporary data stub for feature settings table
 *
 * TODO: Remove when feature settings data is ready.
 * See https://gitlab.com/gitlab-org/gitlab/-/issues/473005#note_2076997154
 */

/* eslint-disable @gitlab/require-i18n-strings */
const stubbedAiFeatureSettings = [
  {
    feature: 'code_generations',
    title: 'Code Generation',
    mainFeature: 'Code Suggestions',
    provider: 'self_hosted',
    selfHostedModel: null,
  },
  {
    feature: 'code_completions',
    title: 'Code Completion',
    mainFeature: 'Code Suggestions',
    provider: 'vendored',
    selfHostedModel: null,
  },
  {
    feature: 'duo_chat',
    title: 'Duo Chat',
    mainFeature: 'Duo Chat',
    provider: 'self_hosted',
    selfHostedModel: null,
  },
];
/* eslint-enable @gitlab/require-i18n-strings */

export default stubbedAiFeatureSettings;
